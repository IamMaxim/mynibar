import styles from "./styles.jsx";

let style = {
  "padding-left": "6px",
  "padding-right": "6px",
  "border-bottom": `${styles.borderSize} solid ${styles.colors.clock_underline}`
}

const render = ({ output }) => {
  if (typeof output === "undefined") return null;
  return (
    <div style={style}>
      
      {/* {output.date} */}
      &nbsp;
      {output.time}
    </div>
  );
};

export default render;
