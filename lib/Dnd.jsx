let style = {
  paddingRight: "8px",
  fontSize: "13px",
  lineHeight: "18px",
}

const render = ({ output }) => {
  if (output === 0) return null;
  return <div style={style}></div>;
};

export default render;
