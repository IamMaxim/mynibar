import styles from "./styles.jsx";

let style = {
  "padding-left": "6px",
  "padding-right": "6px",
  "border-bottom": `${styles.borderSize} solid ${styles.colors.wifi_underline}`,
}

const render = ({ output }) => {
  if (typeof output === "undefined") return null;
  const status = output.status;
  const ssid = output.ssid;
  if (status === "inactive") return <div>􀙈</div>;
  return <div style={style}> {output.ssid}</div>;
};

export default render;
