import styles from "./styles.jsx";

const render = ({ output }) => {
  if (typeof output === "undefined") return null;

  let style = {
    "border-bottom": `${styles.borderSize} solid ${styles.colors.music_underline}`,
    "padding-left": "6px",
    "padding-right": "6px",
  }

  return (
    <div style={style}>
      <span> {output}</span>
    </div>
  );
};

export default render;
