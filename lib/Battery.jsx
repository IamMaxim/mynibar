import styles from "./styles.jsx";

const render = ({ output }) => {
  let charging = output.charging;
  let percentage = output.percentage;
  let remaining = output.remaining;

  let style = {
    "padding-left": "6px",
    "padding-right": "6px",
    "border-bottom": `${styles.borderSize} solid ${styles.colors.battery_underline}`
  }

  if (percentage < 30)
    style.color = styles.colors.red
  else
    style.color = null

  if (charging)
    style.color = styles.colors.green

  return (
    <div>
      <div style={style}>
        <span>{charging ? "" : ""} {percentage}%</span>
      </div>
    </div>
  );
};

export default render;
