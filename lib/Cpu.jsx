import styles from "./styles.jsx";

const render = ({ output }) => {
  if (typeof output === "undefined") return null;

  let style = {
    "border-bottom": `${styles.borderSize} solid ${styles.colors.cpu_underline}`,
    "padding-left": "6px",
    "padding-right": "6px",
  }
  
  if (output.loadAverage > 3)
    style.color = styles.colors.red
  else
    style.color = null

  return (
    <div style={style}>
      <span> {output.cpuTemp}</span>
    </div>
  );
};

export default render;
