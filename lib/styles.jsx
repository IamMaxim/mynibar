export default {
  colors: {
    fg: "rgba(255,255,255,1)",
    dim: "rgba(255,255,255,0.8)",
    // bg: "#1c1c1c",
    bg: "#252525",
    red: "#ff8700",
    green: "#51f35f",
    clock_underline: "#bdbd42",
    battery_underline: "#f86c37",
    wifi_underline: "#7060ff",
    cpu_underline: "#4ae1d6",
    music_underline: "#53e14a",
  },
  borderSize: "1px",
  fontSize: "12px",
  statusLineHeight: "17px",
  spacesLineHeight: "20px",
  fontWeight: 500,
  fontFamily: "'JetBrains Mono Medium', 'Font Awesome 5 Pro', monospace"
}
