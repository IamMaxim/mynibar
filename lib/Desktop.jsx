import styles from "./styles.jsx";
import run from "uebersicht";

const containerStyle = {
  display: "grid",
  gridAutoFlow: "column",
  gridGap: "8px"
};

const desktopStyle = {
  width: styles.spacesLineHeight,
  height: styles.spacesLineHeight,
  lineHeight: styles.spacesLineHeight,
  textAlign: "center",
  position: "relative",
};


const renderSpace = (index, focused, visible) => {
  let contentStyle = JSON.parse(JSON.stringify(desktopStyle))
  
  if (focused == 1) {
    contentStyle.color = styles.colors.fg
    contentStyle.background = styles.colors.dim
    contentStyle.borderRadius = "20px"
    contentStyle.color = styles.colors.bg
  } else if (visible == 1) {
    contentStyle.color = styles.colors.fg
  }

  return (
    <div style={contentStyle}>
      <span>{focused ? index : ""}</span>
    </div>
  );
};

const render = ({ output }) => {
  if (typeof output === "undefined") return null;

  const spaces = [];

  output.forEach(function(space) {
    spaces.push(renderSpace(space.index, space.focused, space.visible));
  });

  return (
    <div style={containerStyle}>
      {spaces}
    </div>
  );
};

export default render;
