import DateTime from "./lib/DateTime.jsx";
import Battery from "./lib/Battery.jsx";
import Music from "./lib/Music.jsx";
import Cpu from "./lib/Cpu.jsx";
import Wifi from "./lib/Wifi.jsx";
import Dnd from "./lib/Dnd.jsx";
import Error from "./lib/Error.jsx";
import parse from "./lib/parse.jsx";
import styles from "./lib/styles.jsx";

const style = {
  display: "grid",
  padding: "0 0 0 12px",
  gridAutoFlow: "column",
  gridGap: "8px",
  position: "fixed",
  overflow: "hidden",
  margin: "0",
  paddingBottom: "-1px",
  borderRadius: "80px 0 0 80px",
  background: styles.colors.bg,
  border: "1px solid #333",
  borderRight: null,
  paddingTop: "2px",
  right: "0px",
  top: "0px",
  color: styles.colors.dim,
  fontFamily: styles.fontFamily,
  fontSize: styles.fontSize,
  lineHeight: styles.statusLineHeight,
  fontWeight: styles.fontWeight
};

export const refreshFrequency = 1000;

export const command = "./nibar/scripts/status.sh";

export const render = ({ output }) => {
  const data = parse(output);
  if (typeof data === "undefined") {
    let msg = "Error: unknown script output " + output
    return (
      <div style={style}>
        <Error msg={msg} side="right" />
      </div>
    );
  }
  return (
    <div style={style}>
      <Music output={data.music}></Music>
      <Cpu output={data.cpu} />
      <Wifi output={data.wifi} />
      <Battery output={data.battery} />
      <DateTime output={data.datetime} />
      <Dnd output={data.dnd} />
    </div>
  );
};

export default null;
