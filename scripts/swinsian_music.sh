#!/usr/bin/osascript

on is_running(appName)
    tell application "System Events" to (name of processes) contains appName
end is_running

tell application "Swinsian"
	-- get the currently playing track
	set thetrack to current track

	-- get properties of the track
	set trackname to name of thetrack
	set trackartist to artist of thetrack
	-- set trackalbum to album of thetrack

	set info to trackartist & " - " & trackname

end tell
